// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    ASSERT_EQ(x[0], 992);
}

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;

    const allocator_type x;  // read-only
    ASSERT_EQ(x[0], 992);
}

// test single alloc
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);

    x.allocate(5);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], 964);
    ASSERT_EQ(x[996], 964);
}

// test two allocs, should be located one after other
TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    x.allocate(5);
    x.allocate(3);
    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -12);
    ASSERT_EQ(x[44], -12);
    ASSERT_EQ(x[48], 944);
    ASSERT_EQ(x[996], 944);
}

// test alloc that needs to fill up exactly the heap size
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    x.allocate(248);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);
}

// test alloc that needs to fill up more than the given size
TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    x.allocate(246);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);
}

// test alloc size 0
TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<double, 1000>;

    allocator_type x;  // read/write
    try {
        x.allocate(0);
    } catch (std::exception& e) {
        ASSERT_EQ(x[0], 992);
        ASSERT_EQ(x[996], 992);
    }
}

// test filling up heap with small blocks
TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    for (int i = 0; i < 50; i++) {
        x.allocate(3);   // each should take up 20 bytes of space
        ASSERT_EQ(x[i * 20], -12);
        ASSERT_EQ(x[(i + 1) * 20 - 4], -12);
    }
}

// test deallocating a single allocation
TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5 = x.allocate(5);
    x.deallocate(alloc_5, 5);

    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

// test deallocating an inner allocation
TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5 = x.allocate(5);
    x.allocate(3);
    x.deallocate(alloc_5, 5);

    ASSERT_EQ(x[0], 20);
    ASSERT_EQ(x[24], 20);
    ASSERT_EQ(x[28], -12);
    ASSERT_EQ(x[44], -12);
    ASSERT_EQ(x[48], 944);
    ASSERT_EQ(x[996], 944);
}

// test deallocating an outer allocation, merge right
TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    x.allocate(5);
    pointer alloc_3 = x.allocate(3);
    x.deallocate(alloc_3, 3);

    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], 964);
    ASSERT_EQ(x[996], 964);
}

// test merge left
TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5_1 = x.allocate(5);
    pointer alloc_5_2 = x.allocate(5);
    x.allocate(5);

    x.deallocate(alloc_5_1, 5);
    x.deallocate(alloc_5_2, 5);

    ASSERT_EQ(x[0], 48);
    ASSERT_EQ(x[52], 48);
    ASSERT_EQ(x[56], -20);
    ASSERT_EQ(x[80], -20);
    ASSERT_EQ(x[84], 908);
    ASSERT_EQ(x[996], 908);
}

// test merge both ways
TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5_1 = x.allocate(5);
    pointer alloc_5_2 = x.allocate(5);
    pointer alloc_5_3 = x.allocate(5);
    x.allocate(5);

    x.deallocate(alloc_5_1, 5);
    x.deallocate(alloc_5_3, 5);
    x.deallocate(alloc_5_2, 5);

    ASSERT_EQ(x[0], 76);
    ASSERT_EQ(x[80], 76);
    ASSERT_EQ(x[84], -20);
    ASSERT_EQ(x[108], -20);
    ASSERT_EQ(x[112], 880);
    ASSERT_EQ(x[996], 880);
}

// test reallocation in same place
TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5 = x.allocate(5);
    x.allocate(5);
    x.deallocate(alloc_5, 5);
    x.allocate(5);

    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -20);
    ASSERT_EQ(x[52], -20);
    ASSERT_EQ(x[56], 936);
    ASSERT_EQ(x[996], 936);
}

// test smaller reallocation in same place
TEST(AllocatorFixture, test16) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5 = x.allocate(5);
    x.allocate(5);
    x.deallocate(alloc_5, 5);
    x.allocate(3);

    ASSERT_EQ(x[0], -20);
    ASSERT_EQ(x[24], -20);
    ASSERT_EQ(x[28], -20);
    ASSERT_EQ(x[52], -20);
    ASSERT_EQ(x[56], 936);
    ASSERT_EQ(x[996], 936);
}

// test deallocation of allocation that was bigger than specified size
TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_246 = x.allocate(246);
    ASSERT_EQ(x[0], -992);
    ASSERT_EQ(x[996], -992);

    x.deallocate(alloc_246, 246);

    ASSERT_EQ(x[0], 992);
    ASSERT_EQ(x[996], 992);
}

// test skipping first alloc because insufficient space
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<int, 1000>;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;  // read/write
    pointer alloc_5 = x.allocate(5);
    x.allocate(5);
    x.deallocate(alloc_5, 5);
    x.allocate(6);

    ASSERT_EQ(x[0], 20);
    ASSERT_EQ(x[24], 20);
    ASSERT_EQ(x[28], -20);
    ASSERT_EQ(x[52], -20);
    ASSERT_EQ(x[56], -24);
    ASSERT_EQ(x[84], -24);
    ASSERT_EQ(x[88], 904);
    ASSERT_EQ(x[996], 904);
}

// test invalid alloc bc not enough space
TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<int, 1000>;

    allocator_type x;  // read/write
    try {
        x.allocate(250);
    } catch (std::exception& e) {
        ASSERT_EQ(x[0], 992);
        ASSERT_EQ(x[996], 992);
    }
}