// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <iostream>
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return *lhs != *rhs;
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int block_size = std::abs(*_p);
            _p = (int*) ((char*) _p + block_size + 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int* prev_block_end = (int*) ((char*) _p - sizeof(int));
            int prev_block_size = *prev_block_end;
            _p = (int*) ((char*) _p - prev_block_size - 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return *lhs == *rhs;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return *lhs != *rhs;
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int block_size = std::abs(*_p);
            _p = (int*) ((char*) _p + block_size + 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int* prev_block_end = (int*) ((char*) _p - sizeof(int));
            int prev_block_size = *prev_block_end;
            _p = (int*) ((char*) _p - prev_block_size - 2 * sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * makes sure that the heap is valid after operations
     */
    bool valid () const {
        auto b = begin();
        auto e = end();

        unsigned int curr_block_size;
        bool is_free = false;

        while (b != e) {
            curr_block_size = std::abs(*b);
            
            // checks no two free blocks are next to each other
            assert(!(is_free && *b > 0));

            // checks that block size stays within heap
            assert(curr_block_size < N);

            // checks that block size is big enough
            assert(curr_block_size >= sizeof(T));

            is_free = *b > 0;
            ++b;
        }

        // checks that we can get to the end
        assert(b == e);

        return true;
    }

    // returns the index of the first free block
    int find_free_block(size_type &needed) {
        unsigned int idx = 0;
        int curr = (*this)[idx];

        while (!(idx < N && curr >= (int) needed)) {
            idx += (std::abs(curr) + 2 * sizeof(int));
            if (idx >= N) break;
            curr = (*this)[idx];
        }

        if (idx >= N) {
            return -1;
        }

        size_type minimum_block_size = 2 * sizeof(int) + sizeof(T);
        if (curr - needed < minimum_block_size) {
            needed = curr;
        }

        return idx;
    }

    // merges the free block on its left (if applicable)
    void merge_left_free_block(int* p) {
        if (p == (int*) a) return;
        int* left_block_end = (int*) ((char*) p - sizeof(int));
        if (*left_block_end < 0) return;

        // left block is free
        int left_block_size = *left_block_end;
        int total_size = left_block_size + *p + 2 * sizeof(int);
        int* left_block_start = (int*) ((char*) p - left_block_size - 2 * sizeof(int));
        int* curr_block_end = (int*) ((char*) p + *p + sizeof(int));

        *left_block_start = total_size;
        *curr_block_end = total_size;
    }

    // merges the free block on its right (if applicable)
    void merge_right_free_block(int* p) {
        int* heap_bound = (int*) (a + N);
        int* right_block_start = (int*) ((char*) p + *p + 2 * sizeof(int));

        if (right_block_start == heap_bound) return;
        if (*right_block_start < 0) return;

        // right block is free
        int right_block_size = *right_block_start;
        int total_size = *p + right_block_size + 2 * sizeof(int);
        int* curr_block_start = p;
        int* right_block_end = (int*) ((char*) right_block_start + right_block_size + sizeof(int));

        *curr_block_start = total_size;
        *right_block_end = total_size;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }

        (*this)[0]   = N-8;
        (*this)[N-4] = N-8;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // prints heap for debugging
    void print() {
        std::cout << "=== printing heap ===" << std::endl;

        unsigned int idx = 0;
        int curr = (*this)[idx];
        while (idx < N) {
            std::cout << idx << ":\t" << curr << std::endl;
            idx += (std::abs(curr) + 2 * sizeof(int));
            std::cout << idx - 4 << ":\t" << (*this)[idx - 4] << std::endl;
            if (idx >= N) break;
            curr = (*this)[idx];
        }

        std::cout << "=====================" << std::endl;
    }

    // prints the sentinels for final output
    void print_sentinels(std::ostream& sout) {
        auto b = begin();
        auto e = end();
        --e;

        while (b != e) {
            sout << *b++ << " ";
        }
        sout << *b << std::endl;
    }

    // finds the nth busy block to deallocate
    pointer find_block(int block) {
        unsigned int idx = 0;
        int curr = (*this)[idx];
        if (curr < 0) block--;

        while (block != 0) {
            idx += (std::abs(curr) + 2 * sizeof(int));
            if (idx >= N) break;
            curr = (*this)[idx];

            if (curr < 0) block--;
        }

        return (T*) &((*this)[idx + sizeof(int)]);
    }

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        // find first free block
        // split the block into allocated and free
        // return the empty space

        size_type needed = n * sizeof(T);
        int first_free = find_free_block(needed);

        if (n == 0 || first_free == -1) {
            throw std::bad_alloc();
        }

        // mark the block as busy
        int prev_value = (*this)[first_free];
        (*this)[first_free] = -needed;
        (*this)[first_free + needed + sizeof(int)] = -needed;

        int leftover_idx = first_free + needed + 2 * sizeof(int);
        int leftover_space = prev_value - needed - 2 * sizeof(int);

        // if there is leftover space, mark the space with sentinels
        if (leftover_space > 0) {
            (*this)[leftover_idx] = leftover_space;
            (*this)[leftover_idx + leftover_space + sizeof(int)] = leftover_space;
            assert((leftover_idx + leftover_space + sizeof(int)) == (first_free + prev_value + sizeof(int)));
        }

        assert(valid());
        return (T*) &((*this)[first_free + sizeof(int)]);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);
        assert(valid());
    }

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type) {
        // check to-be deallocated block
        // deallocate it
        // check left and right for merge

        assert((char*) p > a);
        int* block_start = (int*) ((char*) p - (char*) sizeof(int));

        assert(*block_start < 0);

        // mark the block as free
        int block_size = std::abs(*block_start);
        int* block_end = (int*) ((char*) block_start + sizeof(int) + block_size);
        *block_start = block_size;
        *block_end = block_size;

        merge_right_free_block(block_start);
        merge_left_free_block(block_start);

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
