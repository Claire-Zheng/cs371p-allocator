# CS371p: Object-Oriented Programming Allocator Repo

* Name: Claire Zheng

* EID: cz5977

* GitLab ID: Claire-Zheng

* HackerRank ID: Clayuh

* Git SHA: dca8f83004e041c61789f781abb400e9422b7e1b

* GitLab Pipelines: https://gitlab.com/Claire-Zheng/cs371p-allocator/-/pipelines

* Estimated completion time: 15

* Actual completion time: 15

* Comments: N/A
