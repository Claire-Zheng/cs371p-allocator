// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>
#include <vector>

#include "Allocator.hpp"

using namespace std;

void allocator_read(istream& sin, std::vector<int>& input) {
    std::string line;
    while (getline(sin, line) && line != "") {
        input.push_back(std::stoi(line));
    }
}

void allocator_eval(std::vector<int>& input, my_allocator<double, 1000>& allocator) {
    int block;
    for (unsigned int i = 0; i < input.size(); i++) {
        block = input[i];
        if (block > 0) {  // allocate
            // std::cout << "allocating " << block << std::endl;
            allocator.allocate(block);
        } else {          // deallocate n-th block
            // std::cout << "deallocating block " << block << std::endl;
            allocator.deallocate(allocator.find_block(std::abs(block)), 0);
        }
        // allocator.print();
    }
}

void allocator_print(ostream& sout, my_allocator<double, 1000>& allocator) {
    allocator.print_sentinels(sout);
}

void allocator_solve(istream& sin, ostream& sout) {
    std::string line;
    getline(sin, line);
    int num_cases = std::stoi(line);
    assert(num_cases <= 100);

    getline(sin, line); // consume empty line
    for (int i = 0; i < num_cases; i++) {
        std::vector<int> input;
        my_allocator<double, 1000> allocator;

        allocator_read(sin, input);
        assert(input.size() <= 1000);
        allocator_eval(input, allocator);
        allocator_print(sout, allocator);
    }
}

// ----
// main
// ----

int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */

    allocator_solve(cin, cout);
    return 0;
}
